Role Name
=========

This role deploy's CloudForms version 5.9.2.4-1
Depending on the supplied parameters it can be an all in one deployment or a multi region, multizone with stand-alone, external or HA Database

In scope
--------
* 1 cfme appliance with 1 region Default zone and embedded database
* 1 cfme appliance with 1 region Default zone and external database
* 1 cfme appliance with 1 region Default zone and HA database

Not in scope
------------
This will not (yet) deploy the cmfe appliance onto an infrastucture, cloud or container platform.


Requirements
------------

An already installed cmfe appliance, or multiple appliances.
If extra disk for tmp or databases are needed, they must be defined in that specific hostvars. (it cannot dertermine the disk by itself)
The cfme appliance should be registered with subscription manager ( code will be add later to implement this )

Inventory
---------
At this moment a pragmatic approach to determine the regions, zones and settings for that region/zone is done by creating the region in the Inventory file as a group and all zone(s) in that region are also a new group with the naming convention [ <region> _ <zonename> ].
To set for example a timezone in a specific region, just create a group_var.yml for that region.
In the future a datamodel can be made to replace the logics in the inventory.

Role Variables
--------------
All variables used in this role are prepended with the role name in snake case and followed with a " _ "
For variables that are used by multiple roles for cfme the variable is named "cfme_"

Defined variables
*  cfme_region
*  cfme_zone
*  cfme_deploy_dbpass
*  cfme_deploy_dbserver
*  cfme_deploy_initialpasswd
*  cfme_deploy_vxdisklibversion
*  cfme_deploy_firstcf  # Only 1 server can have this variable set.
*  cfme_deploy_multiserver



Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

GPL

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
